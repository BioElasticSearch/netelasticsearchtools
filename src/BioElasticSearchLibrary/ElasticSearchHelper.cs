﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Elasticsearch.Net.ConnectionPool;
using Nest;
using Nest.Resolvers;
using System.Threading.Tasks;

namespace BioElasticSearchLibrary
{
    public class ElasticSearchHelper
    {
        /**
         * The field which holds the name of the organism which the DNA sequence is applicable to.
         */
        public const String FIELD_ORGANISM = "title";
        /**
         * The raw DNA sequence for the organism.
         */
        public const String FIELD_CONTENT = "content";
        /**
         * The date which the DNA sequence was added to the store.
         */
        public const String FIELD_DATE_ADDED = "postDate";
        /**
         * The length of the DNA sequence as stored within the store.
         */
        public const String FIELD_CONTENT_LENGTH = "contentLength";
        /**
         * The filename which the DNA sequence was supplied from.
         */
        public const String FIELD_FILENAME = "filename";
        /**
         * The store/index which is used to hold the DNA sequences.
         */
        private const String INDEX_DEFAULT = "documents";
        /**
         * The store type.
         */
        private const String INDEX_TYPE = "kmer";

        private ElasticClient client;

        //private static ElasticSearchHelper instance = null;

        private static readonly string[] DEFAULT_NODES = {"http://localhost:9200/"};

        public ElasticSearchHelper()
        {
            ElasticSearchHelperSetup(DEFAULT_NODES);

        }

        public ElasticSearchHelper(string[] nodeLocations)
        {

            ElasticSearchHelperSetup(nodeLocations);

        }

        private void ElasticSearchHelperSetup(string[] nodeLocations)
        {
            Uri[] uris = new Uri[nodeLocations.Length];
            for (int i = 0; i < nodeLocations.Length; ++i)
            {
                uris[i] = new Uri(nodeLocations[i]);
            }
            StaticConnectionPool connectionPool = new StaticConnectionPool(uris);
            var settings = new ConnectionSettings(connectionPool).SetTimeout(int.MaxValue);
            client = new ElasticClient(settings);
        }

        /**
         * Upload the requested file into the ElasticSearch Cluster.
         *
         * @param file The file to send to ElastticSearch
         * @return The document ID as stored by ElasticSearch
         * @throws IOException If the file could not be read, or there was an error sending the file to ES.
         */
        public List<String> sendFile(Stream file, string filename)
        {
            var content = buildSendFile(file, filename);

            var response = client.IndexMany(content, INDEX_DEFAULT, INDEX_TYPE);

            List<String> ids = new List<string>();
            foreach (var ir in response.Items)
            {
                ids.Add(ir.Id);
            }
            return ids;
        }

        private String sendFile(SendFile content)
        {
            var response = client.Index(content, t => t.Index(INDEX_DEFAULT).Type(INDEX_TYPE).Timeout(int.MaxValue.ToString()));

            return response.Id;
        }

        /**
         * Upload the requested file into the ElasticSearch Cluster.
         *
         * @param file The file to send to ElastticSearch
         * @return The document ID as stored by ElasticSearch
         * @throws IOException If the file could not be read, or there was an error sending the file to ES.
         */
        public Task<IIndexResponse> sendFileAsync(Stream file, string filename)
        {
            var content = buildSendFile(file, filename);
            var task = client.IndexAsync(content.First(), t => t.Index(INDEX_DEFAULT).Type(INDEX_TYPE));


            return task;
        }

        public Task<String> sendFileAsync2(Stream file, string filename)
        {
            var content = buildSendFile(file, filename).First();
            var task = Task<String>.Factory.StartNew(() => sendFile(content));


            return task;
        }

        public List<String> sendFilesBulk(List<Stream> files, List<string> filenames)
        {

            List<SendFile> content = new List<SendFile>(files.Count);
            for (int i = 0; i < files.Count; ++i)
            {
                content.AddRange(buildSendFile(files[i],filenames[i]));
                Console.WriteLine("Added File to task: " + filenames[i]);
            }
            return sendFilesBulk(content);
        }

        private List<String> sendFilesBulk(List<SendFile> files)
        {

            Console.WriteLine("Sending Files");
            var response = client.IndexMany(files, INDEX_DEFAULT, INDEX_TYPE);

            List<String> ids = new List<string>();
            foreach (var ir in response.Items)
            {
                ids.Add(ir.Id);
            }
            return ids;
        }

        public List<String> sendFilesBulk(IEnumerable<String> files)
        {

            List<SendFile> content = new List<SendFile>(files.Count());
            foreach (string file in files)
            {
                using (StreamReader sr = new StreamReader(file))
                {
                    content.AddRange(buildSendFile(sr.BaseStream, Path.GetFileName(file)));

                }
            }
            return sendFilesBulk(content);
        }



        class SendFile
        {
            public String title { get; set; }
            public string content { get; set; }
            public string postDate { get; set; }
            public int contentLength { get; set; }
            public string filename { get; set; }
            public SendFile(string organisim, DateTime dateAdded, string filename, int contentLength, string content)
            {
                this.title = organisim;
                this.postDate = dateAdded.ToString("s");
                this.filename = filename;
                this.contentLength = contentLength;
                this.content = content;
            }

        }


        private IEnumerable<SendFile> buildSendFile(Stream file, string filename)
        {
            var sequences = DNASequence.getSequences(file);
            foreach (var seq in sequences)
            {
                yield return new SendFile(seq.getOrganismName(), DateTime.Now, filename, seq.getDNASequenceLength(), seq.getDNASequence());
            }
        }


        public Tuple<string,string>[] GetAllFiles()
        {
            //var q = Query<dynamic>.QueryString(qs=>qs.Query("*:*"));
            var result = //doQuery(q, false, false, null);
                client.Search<dynamic>(s=>s.Index(INDEX_DEFAULT)/*.Query(q=>q
                        .Match(m=>m.OnField("content").Query("aaaaa"))
                    
                    
                    )*/.Type(INDEX_TYPE).Size(int.MaxValue/4).Source(ss => ss.Exclude("content")));
            var resultCount = result.Hits.Count();
            var i = 0;
            Tuple<string, string>[] results = new Tuple<string, string>[resultCount];
            foreach (var hit in result.Hits)
            {
                //var filename = (Newtonsoft.Json.Linq.JArray)hit.Fields.FieldValuesDictionary["filename"];

                results[i] = new Tuple<string, string>(hit.Source.filename.Value, hit.Id);
                ++i;
            }

            return results;
        }

        /**
     * Perform a search with a single simple match query against ElasticSearch
     *
     * @param term The term to search for
     * @param explain True if the query should return an explanation for the query results.
     * @param termVectors True if the query should attempt to return the vectors for terms found.
     * @param termFrequencies "terms" if frequency values for search terms is to be returned, or the minimum number of terms. (null to disable).
     * @return A SearchRespons from ElasticSearch
     */
    public ISearchResponse<dynamic> searchMatch(String term, bool explain, bool termVectors, String termFrequencies) {
        
        // Break term into singel kmers and create a AND query
        String[] kmers = term.Split(' ');
        
        
        QueryContainer[] container = new QueryContainer[kmers.Length];
        for (int i = 0; i < kmers.Length;++i)
        {
            container[i] = Query<dynamic>.Match(p => p.OnField(FIELD_CONTENT).Query(kmers[i]));
        }
        var query = Query<dynamic>.Bool(q => q.Must(container));
        //QueryContainer query = QueryBuilders.boolQuery();
        // Perform the query.
        return doQuery(query, explain, termVectors, termFrequencies);
    }

    /**
     * Perform a search with a simple span query against ElasticSearch
     *
     * @param term The term to search for
     * @param explain True if the query should return an explanation for the query results.
     * @param termVectors True if the query should attempt to return the vectors for terms found.
     * @param termFrequencies "terms" if frequency values for search terms is to be returned, or the minimum number of terms. (null to disable).
     * @return A SearchRespons from ElasticSearch
     * @throws ApplicationException If the terms term does not consist of 3 parts, 2x terms and a maximum distance.
     */
    public ISearchResponse<dynamic> searchSpan(String term, bool explain, bool termVectors, String termFrequencies) {

        String[] subterms = term.Split(',');

        if (subterms.Length == 0) {
            throw new ApplicationException("The query should consist of at least span query");
        }
        if (subterms.Length == 1) {
            return doQuery(buildSpanQuery(term), explain, termVectors, termFrequencies);
        } else {
            QueryContainer[] container = new QueryContainer[subterms.Length];
            for (int i = 0; i < subterms.Length; ++i)
            {
                container[i] = buildSpanQuery(subterms[i]);
            }
            var query = Query<dynamic>.Bool(q => q.Must(container));
            // Perform the query.
            return doQuery(query, explain, termVectors, termFrequencies);
        }
    }

    /**
     * Create a span query based on the term passed.
     *
     * @param term A search tuple that contains 2 terms and a distance.
     * @return A SpanQuery.
     * @throws ApplicationException An exception is thrown if the search term in not in the correct format.
     */
    private QueryContainer buildSpanQuery(String term)
    {
        // Break term into singel kmers and create a AND query
        String[] kmers = term.Split(' ');
        //if (kmers.length != 3) {
        //    throw new ApplicationException("The query should consist of at least two terms and a distance (integer)");
        //}

        // Attempt to get the span distance.
        int spanDistance;
        try {
            spanDistance = int.Parse(kmers[kmers.Length-1]);
        } catch {
            throw new ApplicationException("The query should consist of at least two terms followed by a distance (integer)");
        }

        SpanQuery<dynamic>[] container = new SpanQuery<dynamic>[kmers.Length - 1];
        for (int i = 0; i < container.Length; ++i)
        {
            container[i] = new SpanQuery<dynamic>().SpanTerm(FIELD_CONTENT, kmers[i]);
        }

        SpanNearQuery snq = new SpanNearQuery();
        snq.Clauses = container;
        snq.Slop = spanDistance;
        snq.InOrder = false;
        

        var query = new QueryContainer(snq);

        return query;
    }

    /**
     * Perform a search with a single More Like This query against ElasticSearch
     *
     * @param term The term to search for
     * @param explain True if the query should return an explanation for the query results.
     * @param termVectors True if the query should attempt to return the vectors for terms found.
     * @param termFrequencies "terms" if frequency values for search terms is to be returned, or the minimum number of terms. (null to disable).
     * @return A SearchRespons from ElasticSearch
     */
    public ISearchResponse<dynamic> searchSimilar(String term, bool explain, bool termVectors, String termFrequencies)
    {
        //// Break term into singel kmers and create a AND query
        var mlt = new MoreLikeThisQuery();
        PropertyPathMarker[] fields = {PropertyPathMarker.Create(FIELD_CONTENT)};
        mlt.Fields = fields;
        mlt.Analyzer = "kmer";
        mlt.LikeText = term;
        mlt.TermMatchPercentage = 0.1;
        mlt.MinTermFrequency = 0;
        mlt.MinDocumentFrequency = 0;

        var query = new QueryContainer(mlt);

        // Perform the query.
        return doQuery(query, explain, false, null); // We never return term vectors for a similarity search.
    }

    /**
     * Perform a search with a single Fuzzy Like This query against ElasticSearch
     *
     * @param term The term to search for
     * @param explain True if the query should return an explanation for the query results.
     * @param termVectors True if the query should attempt to return the vectors for terms found.
     * @param termFrequencies True, if the frequencies for ALL terms in the document are to be returned.
     * @return A SearchRespons from ElasticSearch
     */
    public ISearchResponse<dynamic> searchSimilarFuzzy(String term, bool explain, bool termVectors, String termFrequencies)
    {
        var flt = new FuzzyLikeThisQuery();
        PropertyPathMarker[] fields = { PropertyPathMarker.Create(FIELD_CONTENT) };
        flt.Fields = fields;
        flt.Analyzer = "kmer";
        flt.LikeText = term;
        flt.IgnoreTermFrequency = true;
        flt.MinSimilarity = 0.5;

        var query = new QueryContainer(flt);

        // Perform the query.
        return doQuery(query, explain, false, null); // We never return term vectors for a similarity search.
    }

    /**
     * Perform the query using the specified query and parameters.
     *
     * @param query The query which has been built using one of the query builders.
     * @param explain True if the query should return an explanation for the query results.
     * @param termVectors True if the query should attempt to return the vectors for terms found.
     * @param termFrequencies "terms" if frequency values for search terms is to be returned, or the minimum number of terms. (null to disable).
     * @return A SearchRespons from ElasticSearch
     */
    private ISearchResponse<dynamic> doQuery(QueryContainer query, bool explain, bool termVectors, String termFrequencies)
    {

        var response = client.Search<dynamic>(s => s
                .Index(INDEX_DEFAULT)
                .Type("kmer")
                .Fields(FIELD_ORGANISM, FIELD_FILENAME, FIELD_CONTENT_LENGTH, FIELD_DATE_ADDED)
                .Query(query)
                .Explain(explain)
                .Size(10)
                //.Terms(termVectors)
                //.TermFrequencies(termFrequencies)
                );

        return response;
    }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bio.IO.GenBank;
using System.IO;
using Bio;
using System.Text;

namespace BioElasticSearchLibrary
{
    
    

    public class DNASequence
    {
        /**
         * The organismName;
         */
        private String organismName;
        /**
         * The DNA sequence for this organism
         */
        private string dnaSequence;

        public static IEnumerable<DNASequence> getSequences(Stream file)
        {
            GenBankParser gbp = new GenBankParser();
            var parsedFile = gbp.Parse(file).ToList();

            if (parsedFile.Count() == 0)
            {
                throw new IOException("Empty file passed, no sequence available");
            }
            foreach (var seq in parsedFile)
            {
                var dnaSequence = Encoding.UTF8.GetString(seq.ToArray());
                var gbm = (GenBankMetadata)seq.Metadata["GenBank"];
                var organismName = gbm.Source.CommonName;
                yield return new DNASequence(organismName, dnaSequence);

            }
        }
        public static int getLength(Stream file)
        {
            GenBankParser gbp = new GenBankParser();
            return gbp.ParseOne(file).Count();
        }


        public DNASequence(String organismName, String dnaSequence)
        {
            this.organismName = organismName;
            this.dnaSequence = dnaSequence;
        }

        /**
         * Get the Organism Name as contained in the GenBank file.
         *
         * @return The Organism Name
         */
        public String getOrganismName()
        {
            return organismName;
        }

        /**
         * Get the length of the DNA Sequence that this object represents.
         *
         * @return The length of the DNA Sequence.
         */
        public int getDNASequenceLength()
        {
            return dnaSequence.Length;
        }

        /**
         * Get a pointer to the complete DNA sequence.
         *
         * @return The complete DNA sequence. This is a reference to the underlying storage container, so modifications on
         * the return character array will be reflected in further queries.
         */
        public String getDNASequence()
        {
            return dnaSequence;
        }
    }
}
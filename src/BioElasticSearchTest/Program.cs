﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using Nest;
using System.Threading;
using BioElasticSearchLibrary;
using Bio.IO.GenBank;
using Bio;

namespace BioElasticSearchTest
{
    class Program
    {
        static ElasticSearchSingelton client;

        struct fileInfo
        {
            public DateTime start;
            public DateTime end;
            public String ID;
            public bool finished;
            public String status;
            public String path;
            public int Lenght;
            public Task<String> task;
        }

        static void Main(string[] args)
        {

            client = ElasticSearchSingelton.getInstance();

           //if (args.Length < 1)
           //{
           //    Console.WriteLine("Please specifiy a path to the files to be indexed");
           //    return;
           //}

           // //get all files
           //String path = args[0];
           //if (!Directory.Exists(path))
           //{
           //    Console.WriteLine("Error: invalid path please specifiy a path to the files to be indexed");
           //    return;
           //}

           
           // string[] filePaths = Directory.GetFiles(path, "*.gbk", SearchOption.AllDirectories);

            

            //todo delete files

            //todo upload files bulk
            
            //UploadAsync(filePaths);
            //client.sendFilesBulk(filePaths);
            

            //todo index Genes to azure SQL

            var FileAndIds = client.GetAllFiles();
            //IndexGenes(FileAndIds);

            //Test();

            Console.ReadLine();
            
            
        }

        public static void IndexGenes(Tuple<String, String>[] FileAndID)
        {
            Tuple<String, String>[] testTuple = new Tuple<string, string>[3];
            SqlConnection con = new SqlConnection("Server=tcp:f1ub7nda01.database.windows.net,1433;Database=Bio;User ID=QUT@f1ub7nda01;Password=Pass1@34;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            con.Open();
            for (int i = 0; i < FileAndID.Length; ++i)
            {
                Console.WriteLine("file: " + FileAndID[i].Item1);
                List<ISequence> li;
                using (StreamReader sr = new StreamReader(FileAndID[i].Item1))
                {
                    GenBankParser gbp = new GenBankParser();
                    li = gbp.Parse(sr.BaseStream).ToList();

                }
                
                
                SqlTransaction transaction = con.BeginTransaction("geneUpload");
                foreach (ISequence s in li)
                {
                    //Console.WriteLine(s);
                    Sequence seq = (Sequence)s;
                    string str = Encoding.ASCII.GetString(s.ToArray());
                    //string str = new string(s.ToArray());
                    GenBankMetadata gbm = (GenBankMetadata)s.Metadata["GenBank"];
                    List<CodingSequence> lcs = gbm.Features.CodingSequences;
                    foreach (CodingSequence cs in lcs)
                    {
                        if (cs.Location.SubLocations.Count <= 1)
                        {
                            string tag="";
                            if (cs.LocusTag.Count!=0)
                            {
                                tag = cs.LocusTag[0];
                            }
                            else if (cs.ProteinId.Count!=0)
                            {
                                tag = cs.ProteinId[0];
                            }
                            else { throw new Exception("Errro: no locus tag or protein id, trans: " + cs.Translation.Substring(0, 10)); }

                            string sql = string.Format("insert into gene2 (LocationStart, LocationEnd, translation, SourceID, LocusTag) VALUES ('{0}','{1}','{2}','{3}','{4}')",
                                cs.Location.LocationStart, cs.Location.LocationEnd, cs.Translation, FileAndID[i].Item2, tag);
                            //Console.WriteLine(sql);
                            SqlCommand comand = new SqlCommand(sql, con);
                            comand.Transaction = transaction;
                            comand.ExecuteNonQuery();
                            if (cs.LocusTag.Count>1)
                            {
                                Console.Write("Multiple Locus tags: ");
                                foreach (string lst in cs.LocusTag)
                                {
                                    Console.Write(" " + lst);
                                }
                                Console.WriteLine();
                            }
                        }
                        else
                        {
                            Console.WriteLine("Error: A gene is plit over multiple locations");
                        }
                    }

                }
                transaction.Commit();
                Console.WriteLine("file finished: " + FileAndID[i].Item1);
            }
            con.Close();
            Console.ReadLine();
        }

        private static void UploadAsync(string[] filePaths)
        {
            fileInfo[] files = new fileInfo[filePaths.Length];
            for (int i = 0; i < filePaths.Length; ++i)
            {
                files[i].path = filePaths[i];
                files[i].finished = false;
            }




            DateTime start = DateTime.Now;
            int filesLeft = filePaths.Length;
            int filesFinished = 0;
            int currentFile = 0;
            int MAX_THREADS_COUNT = 24;
            //List<Task<IIndexResponse>> responses = new List<Task<IIndexResponse>>();
            //using (StreamWriter sw = new StreamWriter("results.csv"))
            //{
            while (filesLeft > 0)
            {
                while (currentFile - filesFinished < MAX_THREADS_COUNT && currentFile - filesFinished < filesLeft)
                {
                    //todo upload files async


                    using (StreamReader sr = new StreamReader(files[currentFile].path))
                    {
                        Console.WriteLine("Uploading file: {0}", Path.GetFileName(files[currentFile].path));
                        files[currentFile].task = client.sendFileAsync2(sr.BaseStream, Path.GetFileName(files[currentFile].path));
                        files[currentFile].start = DateTime.Now;

                    }
                    currentFile++;

                }


                Thread.Sleep(20);

                for (int i = 0; i < currentFile; ++i)
                {
                    if (!files[i].finished && (files[i].task.Status == TaskStatus.RanToCompletion || files[i].task.Status == TaskStatus.Faulted || files[i].task.Status == TaskStatus.Canceled || files[i].task.Status == TaskStatus.Created))
                    {
                        try
                        {
                            var result = files[i].task.Result;
                            files[i].ID = result;
                            files[i].end = DateTime.Now;
                            files[i].finished = true;
                            files[i].status = files[i].task.Status.ToString();
                            Console.WriteLine("{0} finished in {1}ms with status {2}", result, (DateTime.Now - start).TotalMilliseconds, files[i].task.Status);
                            //sw.WriteLine("{0},{1},{2}", result.Id, (DateTime.Now - start).TotalMilliseconds, files[i].task.Status);
                            //responses.RemoveAt(i);
                        }
                        catch (Exception ex)
                        {
                            //sw.WriteLine(ex.Message);
                            Console.WriteLine("Error: " + ex.Message);
                        }
                        filesLeft--;
                        filesFinished++;
                    }
                }
            }
            Console.WriteLine("Files finished uploading:{0}ms", (DateTime.Now - start).TotalMilliseconds);
            //}

            //get file lengths
            for (int i = 0; i < files.Length; ++i)
            {
                using (StreamReader sr = new StreamReader(files[i].path))
                {
                    files[i].Lenght = DNASequence.getLength(sr.BaseStream);
                }

            }

            using (StreamWriter sw = new StreamWriter("results.csv"))
            {
                foreach (fileInfo fi in files)
                {
                    sw.WriteLine("{0},{1},{2},{3},{4},{5},{6}", fi.path, fi.ID, fi.Lenght, fi.status, fi.start, fi.end, (DateTime.Now - start).TotalMilliseconds);
                }
            }
        }

        static void Test()
        {
            //this was developed against specific queries
            var output = client.searchMatch("aaaaa", false, false, null);
            Console.WriteLine(output.Hits.Count());//should equal 10
            var output4 = client.searchMatch("aaaaaaaaat acccccccccccccg", false, false, null);
            Console.WriteLine(output4.Hits.Count());//should equal 5 (at present) or more once more documents are indexed
            //output.ConnectionStatus.Success
            var output2 = client.searchSpan("tattgcttac aggttatcgc 1000", false, false, null);
            Console.WriteLine(output2.Hits.Count());//Should equal 1

            var output3 = client.searchSimilar("tattgcttacaggttatcg", false, false, null);
            Console.WriteLine(output3.Hits.Count());//should equal 10
        }
    }
}

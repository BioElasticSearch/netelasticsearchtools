﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BioElasticSearchLibrary;

namespace BioElasticSearchTest
{
    class ElasticSearchSingelton : ElasticSearchHelper
    {
        private static ElasticSearchSingelton instance = null;

        private ElasticSearchSingelton(string[] nodeLocations) : base(nodeLocations) 
        { 
        }

        //singleton
        public static ElasticSearchSingelton getInstance()
        {
            if (instance == null)
            {
                //get servers from settings
                string[] nodes = Settings.Default.Servers.Split(',');
                Console.WriteLine("Servers: " + Settings.Default.Servers);
                instance = new ElasticSearchSingelton(nodes);
            }
            return instance;
        }
    }
}
